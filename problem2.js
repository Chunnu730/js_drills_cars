function secondProblem(inventory) {
    let lastindex = inventory.length - 1;
    return "Last car is a " + inventory[lastindex].car_make + " " + inventory[lastindex].car_model;
}

module.exports = secondProblem;