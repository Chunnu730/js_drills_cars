function thirdProblem(inventory) {
    let carModel = [];
    for (let index = 0; index < inventory.length; index++) {
        carModel.push(inventory[index].car_model);
    }

    let flag = false;
    for (; !flag;) {
        flag = true;
        for (let index = 1; index < carModel.length; index++) {
            if (carModel[index - 1] > carModel[index]) {
                flag = false;
                let temp = carModel[index - 1];
                carModel[index - 1] = carModel[index];
                carModel[index] = temp;
            }
        }
    }

    return carModel;
}

module.exports = thirdProblem;