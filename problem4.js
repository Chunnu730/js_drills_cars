function fourthProblem(inventory) {
    let year = [];
    for (let index = 0; index < inventory.length; index++) {
        year[index] = inventory[index].car_year;
    }
    return year;
}
module.exports = fourthProblem;