const fourthProblem = require('./problem4.js');
function fifthProblem(inventory) {
  let year = fourthProblem(inventory);
  let car_older_2000 = [];
  for (let index = 0; index < year.length; index++) {
    if (year[index] < 2000) {
      car_older_2000.push(year[index]);
    }
  }
  let len = car_older_2000.length;
  return car_older_2000;
}
module.exports = fifthProblem;