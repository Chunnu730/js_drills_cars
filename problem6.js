function sixthProblem(inventory) {
    let car = [];
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi") {
            car.push(inventory[i]);
        }
    }
    return car;
}
module.exports = sixthProblem;