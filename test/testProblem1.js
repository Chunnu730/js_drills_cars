const inventory = require('../inventory.js');
const firstProblem = require('../problem1.js');
const result = firstProblem(inventory);
const expectedResult = "Car 33 is a 2011 Jeep Wrangler";
if (expectedResult === result) {
    console.log("Test passed : " + result);
} else {
    console.log("Test Failed");
}

