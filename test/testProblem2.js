const inventory = require('../inventory.js');
const secondProblem = require('../problem2.js');
const result = secondProblem(inventory);
const expectedResult = "Last car is a Lincoln Town Car";
if (expectedResult === result)
    console.log("Test passed : " + result);
else
    console.log("Test Failed");
