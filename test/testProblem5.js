const inventory = require("../inventory.js");
const fifthProblem = require("../problem5.js");
const expectedResult = [1983, 1990, 1995, 1987, 1996, 1997, 1999, 1987, 1995, 1994, 1985, 1997, 1992, 1993, 1964, 1999, 1991, 1997, 1992, 1998, 1965, 1996, 1995, 1996, 1999];
const result = fifthProblem(inventory);
if (JSON.stringify(expectedResult) === JSON.stringify(result) && result.length === 25) {
    console.log("Test passed");
    console.log(result);
    console.log("Length is  " + result.length);
}
else
    console.log("Test failed");